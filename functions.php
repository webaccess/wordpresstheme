<?php
/**
 * _base_ functions and definitions
 *
 * @package _base_
 */

//Base setup
if ( ! function_exists('_base_setup') ) :
function _base_setup() {
	add_theme_support('automatic-feed-links');
	add_theme_support('post-thumbnails');

	register_nav_menus( array(
		'header' => __('Header Menu', '_base_'),
	) );

	add_editor_style( array('css/editor-style.css') );
}
endif;
add_action('after_setup_theme', '_base_setup');


//Disable admin bar
add_filter('show_admin_bar', '__return_false');


//Remove useless metas in head tag
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'rsd_link');


//Enqueue scripts and styles.
function _base_scripts() {
	wp_enqueue_style('_base_-style', get_template_directory_uri() . '/css/style.css', array(), NULL);
	
	wp_enqueue_script('_base_-jquery', get_template_directory_uri() . '/js/vendor/jquery.js', array(), NULL, true);
	wp_enqueue_script('_base_-script', get_template_directory_uri() . '/js/script.js', array('_base_-jquery'), NULL, true);
}
add_action('wp_enqueue_scripts', '_base_scripts');

remove_action( 'login_init', 'send_frame_options_header' );
remove_action( 'admin_init', 'send_frame_options_header' );

include(__DIR__ . '/includes/utils.php');