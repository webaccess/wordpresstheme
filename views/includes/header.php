<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<div class="container_12">
        
        <!-- HEADER --> 	
        <div class="header">
			<?php wp_nav_menu(
				array('theme_location' => 'header')
			); ?>
      	</div>
        <!-- HEADER -->
    </div>