<?php

//category
function w_get_category_id_by_slug($slug) {
	$category = get_category_by_slug($slug);
	return $category->term_id;
}

//lien pour une autres pages 
function w_get_page_link($page_path) {
    return get_permalink(get_page($page_path));
}

//truncate

function truncate($string, $max_length = 999, $replacement = '', $trunc_at_space = true)
{
    $max_length -= strlen($replacement);
    $string_length = strlen($string);

    if($string_length <= $max_length)
        return $string;

    if( $trunc_at_space && ($space_position = strrpos($string, ' ', $max_length-$string_length)) )
        $max_length = $space_position;

    return substr_replace($string, $replacement, $max_length);
}

//date en français
function date_francais($date) {
    return date('d.m.Y', strtotime($date));
}

//debug
function debug($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

//ENCODAGE MAIL
function eae_encode_emails($string) {

    // abort if $string doesn't contain a @-sign
    if (apply_filters('eae_at_sign_check', true)) {
        if (strpos($string, '@') === false) return $string;
    }

    // override encoding function with the 'eae_method' filter
    $method = apply_filters('eae_method', 'eae_encode_str');

    // override regex pattern with the 'eae_regexp' filter
    $regexp = apply_filters(
        'eae_regexp',
        '{
            (?:mailto:)?
            (?:
                [-!#$%&*+/=?^_`.{|}~\w\x80-\xFF]+
            |
                ".*?"
            )
            \@
            (?:
                [-a-z0-9\x80-\xFF]+(\.[-a-z0-9\x80-\xFF]+)*\.[a-z]+
            |
                \[[\d.a-fA-F:]+\]
            )
        }xi'
    );

    return preg_replace_callback(
        $regexp,
        create_function(
           '$matches',
           'return '.$method.'($matches[0]);'
       ),
        $string
    );

}

function eae_encode_str($string) {

    $chars = str_split($string);
    $seed = mt_rand(0, (int) abs(crc32($string) / strlen($string)));

    foreach ($chars as $key => $char) {

        $ord = ord($char);

        if ($ord < 128) { // ignore non-ascii chars

            $r = ($seed * (1 + $key)) % 100; // pseudo "random function"

            if ($r > 60 && $char != '@') ; // plain character (not encoded), if not @-sign
            else if ($r < 45) $chars[$key] = '&#x'.dechex($ord).';'; // hexadecimal
            else $chars[$key] = '&#'.$ord.';'; // decimal (ascii)

        }

    }

    return implode('', $chars);

}